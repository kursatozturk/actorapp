package com.example.deplike


import android.os.AsyncTask
import com.beust.klaxon.Klaxon
import io.github.rybalkinsd.kohttp.ext.httpGet
import okhttp3.Response
import java.util.logging.Logger

class DataFetcher : AsyncTask<Int, Void,  MutableList<Actor>?>() {

    private val url: String = "https://api.themoviedb.org/3/person/popular?"
    private val api_key = "648d97770cc7bbb3d6c0ecf45782b5d3"
    private val Log = Logger.getLogger(MainActivity::class.java.name)
    override fun doInBackground(vararg pages: Int?): MutableList<Actor>? {
        try {

            var page = pages[0]
            val link : String = url + "api_key=$api_key&page=$page"
            Log.warning("Link is : $link")
            val response : Response? = link.httpGet()
            if (response != null) {
                val ActorList = Klaxon().parse<DBResponse>(response.body()!!.string())
                if (ActorList == null){
                    Log.warning("ACTOR LIST IS NULL")
                }
                return ActorList!!.results
            }
            else{
                Log.warning("RESPONSE RETURNED NULL")
            }
        }
        catch (e: Exception){
            return null
        }
        return null
    }
}