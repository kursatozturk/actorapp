package com.example.deplike

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DataHolder.fill(this)

        tabLayout = findViewById(R.id.tabs)
        viewPager = findViewById(R.id.viewPager)
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL


        val fragmentAdapter = MyFragmentAdapter(this, supportFragmentManager, 2)
        viewPager!!.adapter = fragmentAdapter
        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))


        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
        //dataHolder.fillActors(this)
    }
}