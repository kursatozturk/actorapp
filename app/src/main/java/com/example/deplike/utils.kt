package com.example.deplike

import android.content.Context
import android.os.AsyncTask
import com.beust.klaxon.Json
import com.beust.klaxon.Klaxon
import org.jetbrains.annotations.Nullable
import java.io.InputStream

data class DBResponse(
    val results: MutableList<Actor>
) {
    operator fun get(s: String): MutableList<Actor> {
        return this[s]
    }
}

data class Actor(
    //@Nullable
    val name: String? = null,
    //@Nullable
    val popularity: Double? = null,
    //@Nullable
    val profile_path: String? = null
)


data class JsonParser(
    val actors: List<Actor>
)

class Filler: AsyncTask<Context, Void, Void>() {
    override fun doInBackground(vararg context: Context?): Void? {

        DataHolder.instance.fillActors(context[0]!!)
        return null
    }
}

class DataHolder {
    var dataFilled = false
    companion object {
        fun fill(context: Context) {
            Filler().execute(context)
        }
        val instance = DataHolder()
    }
    var map_to_actors: MutableMap<String, MutableList<Actor>> = mutableMapOf()

    fun fillActors (context: Context) {
        val f = context.assets.open("data.json")

        val size = f.available()

        val buffer = ByteArray(size)

        f.read(buffer)
        f.close()

        var data = String(buffer, charset("UTF-8"))

        val ActorList = Klaxon().parse<JsonParser>(data)
        for (actor in ActorList!!.actors){
            this.addActor(actor)
        }
        this.dataFilled = true
    }
    fun addActor (actor: Actor) {
        val first_name = actor.name!!.split(' ')[0] ?: return

        if (this.map_to_actors.containsKey(first_name)) {
            this.map_to_actors[first_name]!!.add(actor)
        }
        else {
            this.map_to_actors.put(first_name, mutableListOf<Actor>(actor))
        }
    }
    fun getActors (actorName: String): List<Actor>? {
        if (map_to_actors.containsKey(actorName))
            return this.map_to_actors[actorName]!!
        else return null
    }
}
