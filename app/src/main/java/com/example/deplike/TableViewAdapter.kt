package com.example.deplike

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class MyAdapter(private val context: Context,
                       private val dataSource: MutableList <Actor>?): BaseAdapter() {

    private val imgUrl: String = "https://image.tmdb.org/t/p/w500/"
    private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val rowView = inflater.inflate(R.layout.actor_row, parent, false)

        val actor: Actor = getItem(position)


        val nameView = rowView.findViewById(R.id.ActorName) as TextView
        val popularityView = rowView.findViewById(R.id.ActorPopularity) as TextView
        val imageView = rowView.findViewById(R.id.ActorImage) as ImageView

        nameView.text = actor.name
        popularityView.text = "${actor.popularity}"
        Picasso.get().load(imgUrl + actor.profile_path).placeholder(R.mipmap.ic_launcher).into(imageView)

        return rowView
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItem(position: Int): Actor {
        return dataSource!![position]
    }

    override fun getCount(): Int {
        return dataSource!!.size
    }

    fun addActors (actors: List<Actor>?) {
        this.dataSource!!.addAll(this.dataSource.size -1, actors!!)
    }

}
