package com.example.deplike

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.SearchView
import android.widget.TextView


class SearchFragment : Fragment() {
    lateinit var c: Context
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        c = context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val view = inflater.inflate(R.layout.fragment_search, container, false)
        val searchView: SearchView = view.findViewById(R.id.search_view)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

            @SuppressLint("SetTextI18n")
            override fun onQueryTextSubmit(query: String?): Boolean {
                val textView = view.findViewById<TextView>(R.id.searchResultCount)

                if (!DataHolder.instance.dataFilled) {
                    textView.text = "Data is not ready yet wait for a couple of seconds and try again"
                    return false
                }
                val actors = DataHolder.instance.getActors(query!!)
                if (actors == null) {
                    textView.text = "0"
                    return true
                }
                val size = actors.size
                textView.text = "Found: $size"

                val listView: ListView = view.findViewById(R.id.SearchResults)
                val adapter = MyAdapter(c, actors as MutableList<Actor>?)
                listView.adapter = adapter

                return true
            }

        })

        return view
    }
}