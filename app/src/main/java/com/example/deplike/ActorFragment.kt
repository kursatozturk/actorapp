package com.example.deplike

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ListView


class ActorFragment : Fragment() {
    var page_num = 1
    var adapter: MyAdapter? = null
    var fetcher = DataFetcher()
    lateinit var c: Context
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        c = context!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_actor, container, false)
        val listView: ListView = view.findViewById(R.id.actor_list_view)
        fetcher.execute(page_num)
        val adapter = MyAdapter(c, fetcher.get())
        listView.adapter = adapter


        val button = view.findViewById<Button>(R.id.showNextPage)
        button.setOnClickListener {
            if ( page_num == 1000)
                return@setOnClickListener

            page_num++
            (listView.adapter as MyAdapter).addActors(DataFetcher().execute(page_num).get())
            (listView.adapter as MyAdapter).notifyDataSetChanged()
            //listView.adapter = MyAdapter(this, actors.get()!!)
        }
        return view
    }
}